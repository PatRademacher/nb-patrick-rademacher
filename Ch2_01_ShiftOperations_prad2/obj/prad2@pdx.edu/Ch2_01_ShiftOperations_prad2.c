#include <stdio.h>
#define USERDEF 232 

void print_good();
int main ()
{
 int i = (((USERDEF * 3) *2) / 6) + 1001;
 int j = i - 501;
 int a = 1;
 int b = 2;
 int c = 3;
 int d = 4;
 int e = 5;
 int m = (((j - 500) + 6) - (3*2));
 int n = ((((m*2)/1)/2) << 8);
 int w = (a << b);
 int x = (d >> e);
 int o = (n >> 4);
 int q;
 printf("\n\nHello! This level of CTF will test your knowledge on bit shifting.\n");
 printf("Use GDB and your reverse engineering skills to find the password, which will\n");
 printf("be an int value. One hint: the password requires more than one shift, so\n");
 printf("pay attention to  the registers to track what is going on. The starting value\n");
 printf("which you can use to start your backtracking is: ");
 printf("%d\n\n\n\n", o);
 printf("Please enter password: ");
 scanf("%d", &q);
 if (q == USERDEF)
 {
 print_good();
 }

 else
 {
   printf("\n\nTry again.\n\n");
 }

return 0;
}

void print_good() {
    printf("\n\nGood Job.\n");
}
