//Copyright 2018 Pat Rademacher - New Beginnings, Assignment 1

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>

int main (void)
{
 char type_words[][50] = {"The", "quick", "brown", "fox", "jumps", "over", "the",
                         "lazy", "dog"}; // string array of all necessary words user must type
 char userword[50]; // string that user inputs/scan in regard to word to type
 int rand_numbers[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8}; // array used for a shuffle to generate random numbers/random order of numbers
 int p = 9; 
 int  z;
 int n = 1;
 struct timeval t, start, end, difference; // structs from sys/time.h library
 printf("%s", "Hello, please try to type all the words correctly - you will be timed!");
 printf("\n\n");

srand(time(&t.tv_usec)); // t.tv_usec is seeded for srand() function
for (int i = 0; i < 9; i++)
{
 int temp = rand_numbers[i];
 int newIndex = rand() % p; // generates a random number starting at 9 and decreased by one for each loop
 rand_numbers[i] = rand_numbers[newIndex]; // switches the value in the array with a value in a different spot of the array 
 rand_numbers[newIndex] = temp;;
 int p = p -1; 
 printf("\n%d", rand_numbers[i]); // -- taken from
	                        // stackoverflow user Brian Tracy 10/3/2018
	                        // *this is a representation of Fisher-Yates shuffle which allows for all possible permutations
 
}

gettimeofday(&start, NULL); // function in library to time
for (int i = 0; i < 9; i++)
{
 printf("%s", "Please type: ");
 printf("%s\n", type_words[rand_numbers[i]]);
 scanf("%s", userword);
 int actual_length = strlen(type_words[rand_numbers[i]]);
 int user_length = strlen(userword);
 for (int u = 0; u < actual_length; u++)
 {
  if (type_words[rand_numbers[i]][u] != userword[u] || user_length > actual_length)
  {
   printf("Misspelled! Try again:\n\n"); // this if statement ensures the user's input matches the word displayed
                                         // by comparing each character as well as if the length of the inputted string
					 // is longer than the string of the word displayed
   scanf("%s", userword);
   user_length = strlen(userword);
   u = 0;
  } 
 }
}
gettimeofday(&end, NULL); // function in libary to end time
timersub(&start, &end, &difference); // function to measure difference in time
printf("\nTotal time to complete in seconds: "); // displays time it took for user to complete in seconds
printf("%li", (difference.tv_sec) * -1);
printf("\nTotal time to complete in u_seconds: "); // displays time it took for user to complete in u_seconds
printf("%li", difference.tv_usec);
printf("\n\n");
return 0;
}
