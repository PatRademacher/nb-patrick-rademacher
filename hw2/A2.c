// Copyright (c) 2018 - Pat Rademacher
// Professor Wu-Chang Feng and Sascha Strand
// Portland State University - New Beginnings
// Assignment A2
// October 15, 2018

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void insertdata (int * exp, int * frac, int * hex);

void assign_sign_exp_frac_and_bias (int * sign, float * EXP, float * FRAC, 
int * exp, int * frac, int * hex, float * bias);

void assign_M_E_and_final (int * sign, float * EXP, float * FRAC, float * bias, 
float * M, float * E, float * final, int * frac, int * exp, int * hex);

int main (void)
{

 int exp, frac, sign, hex;
 float FRAC, EXP, bias, M, E, final;

 insertdata (&exp, &frac, &hex);
 
 assign_sign_exp_frac_and_bias(&sign, &EXP, &FRAC, &exp, &frac, &hex, &bias);
 
 assign_M_E_and_final (&sign, &EXP, &FRAC, &bias, &M, &E, &final, &frac, 
 &exp, &hex);
 
 printf("\n\n");
}

void insertdata (int * exp, int * frac, int * hex)
{
 printf("%s", "Please enter the fraction value:  ");
 scanf("%d\n", frac);
 printf("%s", "Please enter the exponent value:  ");
 scanf("%d\n", exp);
 printf("%s", "Please enter the hexadecimal value:  ");
 scanf("%x", hex);
 if (* frac < 2 || * frac > 10)
 {
  printf("%s", "Sorry, frac value must be between 2 and 10. Please run" 
  " the program again.\n\n");
  exit (0);
 }

// program automatically exits if user inputs
// less than 2 or more than 10 for the fraction and/or
// less than 3 or more than 5 more the exponent.
 
 if (* exp < 3 || * exp > 5)
 {
  printf("\n%s", "Sorry, exponent value must be between 3 and 5. Please run" 
  " the program again.\n\n");
  exit (0);
  }
}

void assign_sign_exp_frac_and_bias (int * sign, float * EXP, float * FRAC,
		               int * exp, int * frac, int * hex, float * bias)
{
 *FRAC = 0;
 * sign = (* hex >> (*exp + *frac) & 1);
           
// this expression above does a bit shift to find the sign value
// by adding the fraction bit number and expression bit number
// and doing an 'and' comparison to determine if it is a
// zero or a one.

 int p = 1;
 int q = -1;
 int r = *frac;
 int s = 0;
 while (p <= * frac)
 {
   *FRAC += (*hex >> (*frac - p) & 1) * (pow(2, q));
   p = p + 1;
   q = q - 1;
         
// this while loop finds the proper integers 
// via a bit shift for the fraction portion and 
// adds all the values by taking those
// numbers after raising them to their proper negative 
// exponent. The same concept is down below but for the 
// exponent bit.  
 }


 while (r <= ((* exp + *frac) - 1))
 {	 
   *EXP += ((*hex >> r) & 1) * (pow(2, s));
   r = r + 1;
   s = s+1;
 }
  *bias = (pow(2, (*exp - 1))) - 1;
}

void assign_M_E_and_final (int * sign, float * EXP, float * FRAC, float * bias,
float * M, float * E, float * final, int * frac, int * exp, int * hex)
{
 int w = 0;
 int r = *frac;
 while (r <= ((*exp + *frac) - 1))
 {
  w += ((*hex >> r) & 1);
  r = r + 1;
 }
 
 w = (w / (*exp));
           
// w adds all the values of the exponent bit field (literally
// adds the ones and zeros) and then is divided by the 
// the total number of bits in the exponent field to determine if they're all 
// ones or not.  

 if (*EXP == 0)
 {
  *E = 1 - *bias;
  *M = *FRAC;
  *final = *M * (pow(2, *E));
  printf("\n%f", *final);
 }

 else if ((w == 1) && (*FRAC == 0) && (*sign == 0))
 {
  printf("\n%s", "Infinity");
 }

 else if ((w == 1) && (*FRAC == 0) && (*sign == 1))
 {
  printf("\n%s", "Negative Infinity");
 }

 else if ((w == 1) && (*FRAC != 0))
 {
  printf("\n%s", "NaN (not a number)");
 }
                        
// Simply put, all the if and else if statements
// are used to find the different scenarios
// of the final value, covering Nan, Negative
// Infinity, Infinity, Denormalized Numbers,
// and the 'Regular Situation.'
 
 else
 {
  *M = 1 + *FRAC;
  *E = *EXP - *bias;
  if (*sign == 0)
  {
   *final = *M * (pow(2, *E));
   printf("\n%f", *final);
  }

  else if (*sign == 1)
  {
   *final = *M * (pow(2, *E)) * -1;
   printf("\n%f", *final);
  }
 }
}
