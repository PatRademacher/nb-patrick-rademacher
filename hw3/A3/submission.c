// Copyright (c) Pat Rademacher 2018
// Assignment A3 
// Wu-chang Feng and Sascha Strand

#include <stdbool.h>
#include "vectors.h"
#include <stdio.h>
bool vector_fma(struct doubleVector * a, const struct doubleVector * b, const struct doubleVector * c)
{
    int i;
    int length = a-> length;
    double * a_data = a->data;
    double * b_data  = b->data;
    double * c_data = c->data;
    int limit = (length/4);
     if (b->length != length || c->length != length){	
     return false;}
	 

    for (i; i < (limit -1); i++)
   {
        __asm__ volatile (
      "1:\n"
  
       "vmovupd (%0), %%ymm0\n" 
       "vmovupd (%1), %%ymm1\n"
       "vmovupd (%2), %%ymm2\n"
       "vfmadd231pd %%ymm1, %%ymm2, %%ymm0\n"
       "vmovupd %%ymm0, (%0)\n"
       "addq $32, %0\n"
       "addq $32, %1\n"
       "addq $32, %2\n"
    :  "+r" (a_data)
    :  "r" (b_data), "r" (c_data)// ,//"+c" (length)
    :  "ymm0", "ymm1", "ymm2"
    );
     } 


    for(i = i*4; i < length; i++)
{
	a_data[i] += b_data[i]*c_data[i];

}
}

