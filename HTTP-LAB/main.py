import requests

p = []

def create_lang_abbr_map():
    """
    This function creates a dictionary that maps language abbreviations,
    "codes", used by detectlanguage.com to full language names.
    """
    r = requests.get("https://ws.detectlanguage.com/0.2/languages")
    return {x["code"]: x["name"] for x in r.json()}


lang_abbr_map = create_lang_abbr_map()


def get_advice():
    r = []
    for i in range(50):
        r.append(requests.get('https://api.adviceslip.com/advice'))
    slip_id = []
    for slip in r:
            x = slip.json()
            advice = x['slip']
            slip_num = advice['slip_id']
            slip_id.append(slip_num)
    print("The most popular advice ticket is number:", find_most_popular_advice(slip_id))
    count = 0
    var = find_most_popular_advice(slip_id)
    for i in slip_id:
        if i == var:
            count = count + 1
    print("The number of times this advice slip is in the list is: ", count)
def find_most_popular_advice(advice_list):
    """
    The `key` parameter of the `max` function takes a function to be
    applied to each element in the list. The `max` function then
    applies the key function to each element in the list with the higest
    result of the application of the key function.
    """
  
    return max(advice_list, key=lambda x: advice_list.count(x))
def format_lang_string(lang_str):
    """
    This function formats a string to be a parameter in a POST request
    to https://detectlanguage.com
    """
    lang_str = lang_str.replace(' ', '+')
    lang_str = 'q=' + lang_str
    return lang_str


def post_language_samples(list1):
    lang_samples = ["Përshendetje Botë", "Русский компьютер", "Ahoj světe",
                    "ਕੰਪਿਊਟਰ ਵਿਗਿਆਨ", "ﻡﺮﺤﺑﺍ ﺏﺎﻠﻋﺎﻠﻣ", "Labas pasauli",
                    "Привет мир", "ਯੇਅ ਨਿਊ ਸ਼ੁਰੂਆਤ", "ہیﻝﻭ ﺪﻧیﺍ",
                    "Salam dünya", "नमस्कार संसार", "Hal heimur",
                    "Беларускія Кампутарныя", "ਸਤਿ ਸ੍ਰੀ ਅਕਾਲ ਦੁਨਿਆ",
                    "Прывітанне Сусвет"]

    for item in lang_samples:
        headers = {"Authorization": "Bearer 80b4ffd09ed8d997433f36a621dc9d62"} #x's represent where API key would go
        params = format_lang_string(item)
       # print(params)
        r = requests.post('https://ws.detectlanguage.com/0.2/detect', headers=headers, params=params)
        list1.append(get_lang_abbr_from_resp(r))
       # print(r.json())
    return lang_samples
def get_lang_abbr_from_resp(http_resp):
    """
    This function takes a requests object containing a response from
    detectlanguage.com, parses it, and returns the abbreviation of
    the language detected.
    """

    return http_resp.json()["data"]["detections"][0]["language"]


if __name__ == "__main__":
    get_advice()
    list_of_abbr = []
    post_language_samples(list_of_abbr)
    maxLangCount = 0
    max_lang = ''
    emptylist = []
    listofstrings = (post_language_samples(emptylist))
    moststrings = []
    for l in list_of_abbr:
        if list_of_abbr.count(l) > maxLangCount:
            max_lang = l
            maxLangCount = list_of_abbr.count(l)
    m = create_lang_abbr_map()
    for i in range(len(list_of_abbr)):
        if (list_of_abbr[i] == max_lang):
            moststrings.append(listofstrings[i])
    print('The most common language in the list is:', m[max_lang], "and it appears ", maxLangCount, " times.")
    print("The strings that are in this language are: ", moststrings)
