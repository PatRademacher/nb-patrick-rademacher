# Copyright (c) Pat Rademacher 2018
# Professor Wu-chang Feng and Sascha Strand
# October 23, 2018

class HeadLight():
    def does(self):
        return "illuminates"


class DriveTrain():
    def does(self):
        return "propels"

class SoundSystem():
    def does(self):
        return "rocks"
class Car():
    def __init__(self, headlight, drivetrain, soundsystem):
        self.headlight = HeadLight()
        self.drivetrain = DriveTrain()
        self.soundsystem = SoundSystem()
    def does(self, headlight, drivetrain, soundsystem):
        print(f'This car {headlight.does()}, {drivetrain.does()}, and {soundsystem.does()}.')

a_headlight = HeadLight()
a_drivetrain = DriveTrain()
a_soundsystem = SoundSystem()

a_car = Car(a_headlight, a_drivetrain, a_soundsystem)

a_car.does(a_headlight, a_drivetrain, a_soundsystem)
