# Copyright (c) Pat Rademacher 2018
# Professor Wu-chang Feng and Sascha Strand
# October 23, 2018

class Bear():
    def eats(self):
        return "berries"

class Turtle():
    def eats(self):
        return "insects and small fish"

class Sasquatch():
    def eats(self):
        return "hazelnuts"

smokey = Bear()
henry = Turtle()
bigfoot = Sasquatch()

print(smokey.eats())
print(henry.eats())
print(bigfoot.eats())
