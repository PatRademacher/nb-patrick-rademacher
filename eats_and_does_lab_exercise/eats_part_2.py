# Copyright (c) Pat Rademacher 2018
# Professor Wu-cheng Fang and Sascha Strand
# October 25, 2018
# used resources from https://thepythonguru.com/python-operator-overloading/

import math

class Bear:
    def __init__(self, firstname):
        self.__firstname = firstname

    def set_firstname(self, firstname):
        self.__firstname = firstname

    def get_firstname(self):
        return (self.__firstname)
    
    def __gt__(self, another_bear):
        return self.__firstname > another_bear.__firstname

    def __lt__(self, another_bear):
        return self.__firstname < another_bear.__firstname
smokey = Bear("Smokey")
yogi = Bear("Yogi")

print(f'Is {smokey.get_firstname()} less than {yogi.get_firstname()}?')
print(smokey < yogi)

class Turtle:
    def __init__(self, gradelevel):
        self.__gradelevel = gradelevel

    def set_gradelevel(self, gradelevel):
        self.__gradelvel = gradelevel

    def get_gradelevel(self):
        return (self.__gradelevel)

    def __gt__(self, another_turtle):
        return self.__gradelevel > another_turtle.__gradelevel

    def __lt__(self, another_turtle):
        return self.__gradelevel < another_bear.__gradelevel

franklin = Turtle(4)
henry = Turtle(3)

print('Is Franklin in a higher grade than Henry?')
print(franklin > henry)

class Sasquatch:
    def __init__(self, phonenumber):
        self.__phonenumber = phonenumber

    def set_phonenumber(self, phonenumber):
        self.__phonenumber = phonenumber

    def get_phonenumber(phonenumber):
        return (self.__phonenumber)

    def __gt__(self, another_sasquatch):
        return self.__phonenumber > another_sasquatch.__phonenumber

    def __lt__(self, another_sasquatch):
        return self.__phonenumber < another_sasquatch.__phonenumber

bigfoot = Sasquatch(5555555555)
friend = Sasquatch(6168663610)

print("Is Bigfoot's phone number greater than Friend's phone number?")
print(bigfoot > friend)
