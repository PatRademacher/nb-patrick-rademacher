#Copyright (c) Pat Rademacher and Aidan Wolfe 2018
#Professor Wu-chang Feng and Sascha Strand
#October 25, 2018

class Electronic():
    count = 0
    def __init__(self, power_state):
        self.power = power_state
        Electronic.count += 1

    def flip_switch(self, on_off):
        self.power = on_off
    def is_on(self):
        print(f"The device {self.name} is {self.power}")
    @classmethod
    def instances(cls):
        print(f"{cls.count} electronics have been created\n")

class HeadLight(Electronic):
    def __init__(self, power_state):
        super().__init__(power_state)
        self.name = "Headlight"

    def does(self):
        return "illuminate"


class DriveTrain():
    def does(self):
        return "propels"

class SoundSystem(Electronic):
    def __init__(self, power_state):
        super().__init__(power_state)
        self.name = "Sound System"

    def does(self):
        return "rocks"
class Car():
    def __init__(self):
        self.head_light = HeadLight("off")
        self.drive_train = DriveTrain()
        self.sound_system = SoundSystem("off")
    def does(self):
        print("the headlights " + self.head_light.does())
        print("the drive train " + self.drive_train.does())
        print("the sound system " + self.sound_system.does())






car1 = Car()
car2 = Car()
car3 = Car()
car1.does()
print("Turning on car1's sound system...")
car1.sound_system.flip_switch("on")
print("Turning on car2's headlights...")
car2.head_light.flip_switch("on")
print("car1 - ", end= '')
car1.sound_system.is_on()
print("car2 - ", end= '')
car2.head_light.is_on()
car1.sound_system.instances()
