# Pat Rademacher and Aidan Wolfe Copyright (c) 2018
# Sascha Strand
# November 20, 2018

def capitalizer(text):
    """Capitalizes the first letter of each word in a string."""
    text_list = list(text)
    x = 0
    b = 2
    if text_list[0].isalnum():
        if text_list[0].isalpha():
            text_list[0] = text_list[0].upper()
    else:
        while not text_list[x].isalpha():
            x += 1
        text_list[x] = text_list[x].upper()
    for a in range(len(text)):
        if (text_list[a - 1] == ' ' or (not text_list[a - 1].isalnum() and text_list[a - b] == ' ')):
            if text_list[a].isalnum():
                if text_list[a].isalpha():
                    text_list[a] = text_list[a].upper()
            elif not text_list[a-1].isalnum() and text_list[a-1] != ' ':
                b = b + 1
            elif text_list[a] == ' ':
                b = 2
    return ''.join(text_list)
