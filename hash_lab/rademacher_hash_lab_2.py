# Copyright (c) 2018 Pat Rademacher
# Sascha Strand
# November 7, 2018
# Hash Lab Pt. 2

universities = [["Portland State University", {"founded":"1956", "Students": 28241}],[
"University of Oregon", {"founded":"1876", "Students": 22980}],[
"Oregon State University", {"founded": "1868", "Students": 30896}]]

class Hash_Map:
    
    def __init__(self):
        self.assoc_array = [""] * 1024
                               # sets array's index in proper range
        for i in range(1024):
            self.assoc_array[i] = ["",""]
                               # fills array with two empty slots -
                               # one for key and one for value
    
    def __add__(self, key, value):
        self.key = key
        self.value = value
        self.key_hashed = hash(self.key)
                               # hashes key string to int value
        self.key_mod = self.key_hashed%1024
                               # mods value by 1024
        self.assoc_array[self.key_mod] = [self.key, self.value]
                               # places key and its value in appropriate slot
                               # based on hash/mod value of key
    
    def __in__(self):
        a = 0
        self.key_checker = input("Type in a university and see if it's in the dictionary!\n")
        for i in range(len(self.assoc_array)):
            if (self.key_checker in self.assoc_array[i][0]):
                               # because key is in first slot of array, for loop and if 
                               # statement only check the 0 spot of each slot in array
                print("Yes, it is!")
                print(self.assoc_array[i])
                a = 1
        if (a != 1):
            print("Sorry, that is not in our dictionary.")
        input()
    
    def __print__(self):
        return self.assoc_array


University_Hash_Map = Hash_Map()

for i in range(len(universities)):
    University_Hash_Map.__add__(universities[i][0], universities[i][1])

University_Hash_Map.__in__()

print(f'{University_Hash_Map.__print__()}')
