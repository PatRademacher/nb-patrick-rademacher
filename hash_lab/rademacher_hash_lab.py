# Copyright (c) Pat Rademacher 2018

def collision_rate(uuid_list):
    sum_list = []
    count = 0
    for i in uuid_list:

        sum_list.append(sum(ord(x) ** i.find(x) for x in i))

    sum_list_w_o_dups = list(set(sum_list))
    difference = len(sum_list) - len(sum_list_w_o_dups)
    collision_rate = difference / len(sum_list) * 100
    return collision_rate

with open('uuid.txt', 'r') as f:
    uuid_full = f.readlines()

print(f"collision rate = {collision_rate(uuid_full)}%")

f.close()
